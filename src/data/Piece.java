/** The class for the chess Pieces that will be used. 
 *
 * @author Peter Lambe
 * @author Le Liu
 * 
 */

package data;

public class Piece {

	public String nameofPiece;
	public Location location;
	public String typeofPiece;
	/** Color of the piece (white or black)
	 * 
	 */
	public String team;

	public Piece(String pieceType, String color, String pieceName, Location position){
		
		super();
		this.typeofPiece = pieceType;
		this.team = color;
		this.nameofPiece = pieceName;
		this.location = position;
		
	}
}