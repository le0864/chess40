/** Class that has the location of various pieces on the board.
 *
 * @author Peter Lambe
 * @author Le Liu
 * 
 */

package data;

public class Location {

	Integer x;
	Integer y;

	public Location(Integer x, Integer y) {
		super();
		this.x = x;
		this.y = y;

	}

	@Override
	public int hashCode() {
		final int primeNumber = 31;
		int answer = 1;
		answer = primeNumber * answer + ((x == null) ? 0 : x.hashCode());
		answer = primeNumber * answer + ((y == null) ? 0 : y.hashCode());
		return answer;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}
		Location different = (Location) object;
		if (x == null) {
			if (different.x != null) {
				return false;
			}
		} else if (!x.equals(different.x)) {
			return false;
		}
		if (y == null) {
			if (different.y != null) {
				return false;
			}
		} else if (!y.equals(different.y)) {
			return false;
		}
		return true;
	}
}