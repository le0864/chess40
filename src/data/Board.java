/** The class the holds all the information and possible moves on the Board.
 *
 * @author Peter Lambe
 * @author Le Liu
 * 
 * 
 */

package data;

import chess.Chess;
import java.util.*;

public class Board {

    public static Piece theBoard[][] = new Piece[8][8];
    static Piece temporary;
    static boolean whiteRookMovedLeft = false;
    static boolean whiteRookMovedRight = false;
    static boolean whiteKingMoved = false;
    static boolean blackRookMovedLeft = false;
    static boolean blackRookMovedRight = false;
    static boolean blackKingMoved = false;
    static boolean check = false;
    static boolean ePW = false;
    static boolean ePB = false;
    static Location enPassantWhite = new Location(null, null);
    static Location enPassantBlack = new Location(null, null);
    static int enPassantWhiteCounter;
    static int enPassantBlackCounter;

    /** 
	 * @param Constructor
	 */
    public Board(Piece board[][]) {

        super();
        board = Board.theBoard;

    }
    
    
    /** Method to generate the board after each turn
   	 * 
   	 */
    public static void drawBoard() {
    	System.out.println("");
        for (int j = 7; j >= 0; j--) {

            for (int i = 0; i < 8; i++) {

                if (theBoard[i][j] != null) {
              
                    System.out.print(theBoard[i][j].nameofPiece);
                    System.out.print(" ");

//                    if (i % 2 == 1 && j % 2 == 1) {
//                        System.out.print(theBoard[i][j].nameofPiece + " ");
//                    } else if (i % 2 == 0 && j % 2 == 0) {
//                        System.out.print(theBoard[i][j].nameofPiece + " ");
//                    } else {
//                        System.out.print(theBoard[i][j].nameofPiece + " ");
//                    }
                } else {
                    if ((i % 2 == 1 && j % 2 == 1) || (i % 2 == 0 && j % 2 == 0) ) {
                        System.out.print("##");
                    } else {
                        System.out.print("  ");
                    }
                    System.out.print(" ");
                }

                if (i == 7) {
                    System.out.print(j + 1);
                }

            }
            System.out.println();

        }

        System.out.print(" a  ");
        System.out.print("b  ");
        System.out.print("c  ");
        System.out.print("d  ");
        System.out.print("e  ");
        System.out.print("f  ");
        System.out.print("g  ");
        System.out.print("h  ");
        System.out.println("");

    }

    /** Method used to determine if the player is in Checkmate
   	 * @param String team - The color (white or black) that may or may not be in checkmate
   	 * @return Boolean of whether or not checkmate is true
   	 */
    public static boolean checkmateState(String team) {

        Piece theLeaderKing = getPositionofKing(team);
        ArrayList<Location> previouslyAttackedPositions = beAttacked(team);

        Location left = new Location(theLeaderKing.location.x - 1, theLeaderKing.location.y);
        Location topLeft = new Location(theLeaderKing.location.x - 1, theLeaderKing.location.y + 1);
        Location top = new Location(theLeaderKing.location.x, theLeaderKing.location.y + 1);
        Location topRight = new Location(theLeaderKing.location.x + 1, theLeaderKing.location.y + 1);
        Location right = new Location(theLeaderKing.location.x + 1, theLeaderKing.location.y);
        Location bottomRight = new Location(theLeaderKing.location.x + 1, theLeaderKing.location.y - 1);
        Location bottom = new Location(theLeaderKing.location.x, theLeaderKing.location.y - 1);
        Location bottomLeft = new Location(theLeaderKing.location.x - 1, theLeaderKing.location.y - 1);

        if (theBoard[theLeaderKing.location.x][theLeaderKing.location.y] != null && previouslyAttackedPositions
                .contains(theBoard[theLeaderKing.location.x][theLeaderKing.location.y].location)) {

            if (theLeaderKing.location.x == 0 && theLeaderKing.location.y == 7) {
                if ((previouslyAttackedPositions.contains(right)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(bottomRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottom)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y - 1] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.x == 7 && theLeaderKing.location.y == 7) {

                if ((previouslyAttackedPositions.contains(left)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(bottomLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottom)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y - 1] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.x == 7 && theLeaderKing.location.y == 0) {

                if ((previouslyAttackedPositions.contains(left)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(topLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(top)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y + 1] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.x == 0 && theLeaderKing.location.y == 0) {

                if ((previouslyAttackedPositions.contains(right)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(topRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(top)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y + 1] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.x == 0) {

                if ((previouslyAttackedPositions.contains(top)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(topRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(right)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(bottomRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottom)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y - 1] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.x == 7) {

                if ((previouslyAttackedPositions.contains(top)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(topLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(left)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(bottomLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottom)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y - 1] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.y == 0) {

                if ((previouslyAttackedPositions.contains(left)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(topLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(top)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(topRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(right)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y] != null)) {
                    return true;
                }
            } else if (theLeaderKing.location.y == 7) {

                if ((previouslyAttackedPositions.contains(left)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(bottomLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottom)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottomRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(right)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y] != null)) {
                    return true;
                }
            } else {

                if ((previouslyAttackedPositions.contains(left)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(topLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(top)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(topRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y + 1] != null)
                        && (previouslyAttackedPositions.contains(right)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y] != null)
                        && (previouslyAttackedPositions.contains(bottomRight)
                        || theBoard[theLeaderKing.location.x + 1][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottom)
                        || theBoard[theLeaderKing.location.x][theLeaderKing.location.y - 1] != null)
                        && (previouslyAttackedPositions.contains(bottomLeft)
                        || theBoard[theLeaderKing.location.x - 1][theLeaderKing.location.y - 1] != null)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /** Used to determine if there is any king still on the board
   	 * @return boolean 
   	 */

    public static boolean kingExists(ArrayList<Location> possibleMoves, String color) {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("king")) {
                    if (color.equalsIgnoreCase("white") && theBoard[i][j].team.equalsIgnoreCase("black")) {

                        if (possibleMoves.contains(theBoard[i][j].location)) {
                            return true;
                        }
                    } else if (color.equalsIgnoreCase("black") && theBoard[i][j].team.equalsIgnoreCase("white")) {

                        if (possibleMoves.contains(theBoard[i][j].location)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    /** Used for the while loop to determine if the King is still alive
   	 * @return Color of which King is still alive
   	 */

    public String kingStillAlive() {

        boolean white = false;
        boolean black = false;

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("king")) {
                    if (theBoard[i][j].team.equalsIgnoreCase("white")) {
                        white = true;
                    }
                    if (theBoard[i][j].team.equalsIgnoreCase("black")) {
                        black = true;
                    }
                }
            }
        }

        if (white && black) {
            return "both";
        } else if (white && !black) {
            return "white";
        } else if (!white && black) {
            return "black";
        } else {
            return null;
        }

    }

    /** Gets the current position of the king
   	 * @param the color of the king you want the position of (white or black)
   	 * @return the King object
   	 */
    public static Piece getPositionofKing(String clear) {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("king")) {

                    if (clear.equalsIgnoreCase("white") && theBoard[i][j].team.equalsIgnoreCase("black")) {

                        return theBoard[i][j];

                    } else if (clear.equalsIgnoreCase("black") && theBoard[i][j].team.equalsIgnoreCase("white")) {

                        return theBoard[i][j];
                    }
                }
            }
        }
        return null;
    }

    /** Used as a way to determine the possible moves 
   	 * @param the color in question (white or black)
   	 * @return an arraylist of possible positions 
   	 */
    public static ArrayList<Location> beAttacked(String team) {

        ArrayList<Location> singleMoves = new ArrayList<Location>();
        ArrayList<Location> currentlyAttackedPositions = new ArrayList<Location>();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("pawn")
                        && theBoard[i][j].team.equalsIgnoreCase(team)) {
                    singleMoves = checkMovesofPawn(theBoard[i][j], theBoard[i][j].location);
                    currentlyAttackedPositions.addAll(singleMoves);
                } else if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("rook")
                        && theBoard[i][j].team.equalsIgnoreCase(team)) {
                    singleMoves = checkMovesofRook(theBoard[i][j], theBoard[i][j].location);
                    currentlyAttackedPositions.addAll(singleMoves);

                } else if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("bishop")
                        && theBoard[i][j].team.equalsIgnoreCase(team)) {
                    singleMoves = checkMovesofBishop(theBoard[i][j], theBoard[i][j].location);
                    currentlyAttackedPositions.addAll(singleMoves);
                } else if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("knight")
                        && theBoard[i][j].team.equalsIgnoreCase(team)) {
                    singleMoves = checkMovesofKnight(theBoard[i][j], theBoard[i][j].location);
                    currentlyAttackedPositions.addAll(singleMoves);
                } else if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("queen")
                        && theBoard[i][j].team.equalsIgnoreCase(team)) {
                    singleMoves = checkMovesofQueen(theBoard[i][j], theBoard[i][j].location);
                    currentlyAttackedPositions.addAll(singleMoves);

                } else if (theBoard[i][j] != null && theBoard[i][j].typeofPiece.equalsIgnoreCase("king")
                        && theBoard[i][j].team.equalsIgnoreCase(team)) {
                    singleMoves = checkMovesofKing(theBoard[i][j], theBoard[i][j].location);

                    currentlyAttackedPositions.addAll(singleMoves);
                }
            }
        }
        return currentlyAttackedPositions;
    }
    
    /** Used to check the valid moves of a Pawn
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> checkMovesofPawn(Piece thePiece, Location firstPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        Location coordinates;

        if (thePiece.team.equalsIgnoreCase("white")) {

            if (ePB && enPassantBlackCounter == Chess.counter && firstPos.x > 0 && enPassantBlack.x < 7
                    && theBoard[enPassantBlack.x + 1][enPassantBlack.y] != null
                    && theBoard[enPassantBlack.x + 1][enPassantBlack.y].nameofPiece.equalsIgnoreCase("wp")
                    && enPassantBlack.x == firstPos.x - 1 && enPassantBlack.y == firstPos.y) {
                coordinates = new Location(firstPos.x - 1, firstPos.y + 1);
                validMoves.add(coordinates);
            }

            if (ePB && enPassantBlackCounter == Chess.counter && firstPos.x < 7 && enPassantBlack.x > 0
                    && theBoard[enPassantBlack.x - 1][enPassantBlack.y] != null
                    && theBoard[enPassantBlack.x - 1][enPassantBlack.y].nameofPiece.equalsIgnoreCase("wp")
                    && enPassantBlack.x == firstPos.x + 1 && enPassantBlack.y == firstPos.y) {
                coordinates = new Location(firstPos.x + 1, firstPos.y + 1);
                validMoves.add(coordinates);
            }

            if (firstPos.y < 7) {

                if (theBoard[firstPos.x][firstPos.y + 1] == null) {

                    coordinates = new Location(firstPos.x, firstPos.y + 1);
                    validMoves.add(coordinates);

                }
            }

            if (firstPos.y == 1) {

                if (theBoard[firstPos.x][firstPos.y + 1] == null && theBoard[firstPos.x][firstPos.y + 2] == null) {
                    coordinates = new Location(firstPos.x, firstPos.y + 2);
                    validMoves.add(coordinates);

                }
            }

            if (firstPos.x != 0 && firstPos.y < 7 && theBoard[firstPos.x - 1][firstPos.y + 1] != null
                    && theBoard[firstPos.x - 1][firstPos.y + 1].team.equalsIgnoreCase("black")) {
                coordinates = new Location(firstPos.x - 1, firstPos.y + 1);
                validMoves.add(coordinates);
            }

            if (firstPos.x != 7 && firstPos.y < 7 && theBoard[firstPos.x + 1][firstPos.y + 1] != null
                    && theBoard[firstPos.x + 1][firstPos.y + 1].team.equalsIgnoreCase("black")) {
                coordinates = new Location(firstPos.x + 1, firstPos.y + 1);
                validMoves.add(coordinates);
            }

        } else {

            if (ePW && Chess.counter == enPassantWhiteCounter && firstPos.x > 0 && enPassantWhite.x < 7
                    && theBoard[enPassantWhite.x + 1][enPassantWhite.y] != null
                    && theBoard[enPassantWhite.x + 1][enPassantWhite.y].nameofPiece.equalsIgnoreCase("bp")
                    && enPassantWhite.x == firstPos.x - 1 && enPassantWhite.y == firstPos.y) {
                coordinates = new Location(firstPos.x - 1, firstPos.y - 1);
                validMoves.add(coordinates);
            }

            if (ePW && Chess.counter == enPassantWhiteCounter && firstPos.x < 7 && enPassantWhite.x > 0
                    && theBoard[enPassantWhite.x - 1][enPassantWhite.y] != null
                    && theBoard[enPassantWhite.x - 1][enPassantWhite.y].nameofPiece.equalsIgnoreCase("bp")
                    && enPassantWhite.x == firstPos.x + 1 && enPassantWhite.y == firstPos.y) {
                coordinates = new Location(firstPos.x + 1, firstPos.y - 1);
                validMoves.add(coordinates);
            }

            if (firstPos.y > 0) {
                if (theBoard[firstPos.x][firstPos.y - 1] == null) {
                    coordinates = new Location(firstPos.x, firstPos.y - 1);
                    validMoves.add(coordinates);
                }
            }

            if (firstPos.y == 6) {

                if (theBoard[firstPos.x][firstPos.y - 1] == null && theBoard[firstPos.x][firstPos.y - 2] == null) {
                    coordinates = new Location(firstPos.x, firstPos.y - 2);
                    validMoves.add(coordinates);
                }
            }

            if (firstPos.x != 0 && firstPos.y > 0 && theBoard[firstPos.x - 1][firstPos.y - 1] != null
                    && theBoard[firstPos.x - 1][firstPos.y - 1].team.equalsIgnoreCase("white")) {
                coordinates = new Location(firstPos.x - 1, firstPos.y - 1);
                validMoves.add(coordinates);
            }

            if (firstPos.x != 7 && firstPos.y > 0 && theBoard[firstPos.x + 1][firstPos.y - 1] != null
                    && theBoard[firstPos.x + 1][firstPos.y - 1].team.equalsIgnoreCase("white")) {
                coordinates = new Location(firstPos.x + 1, firstPos.y - 1);
                validMoves.add(coordinates);
            }

        }
        return validMoves;
    }
    
    /** Used to check the valid moves of a Rook
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> checkMovesofRook(Piece thePiece, Location firstPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        Location coordinates;

        int x = firstPos.x;
        int y = firstPos.y;

        while (x < 8) {
            x += 1;
            if (x < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x >= 0) {
            x -= 1;
            if (x >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (y < 8) {
            y += 1;
            if (y < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (y >= 0) {
            y -= 1;
            if (y >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        return validMoves;
    }
    
    /** Used to check the valid moves of a Knight
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> checkMovesofKnight(Piece thePiece, Location firstPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        Location coordinates;

        if (firstPos.x != 7 && firstPos.y < 6
                && (theBoard[firstPos.x + 1][firstPos.y + 2] == null
                || (theBoard[firstPos.x + 1][firstPos.y + 2] != null
                && theBoard[firstPos.x + 1][firstPos.y + 2].team != thePiece.team))) {
            coordinates = new Location(firstPos.x + 1, firstPos.y + 2);
            validMoves.add(coordinates);

        }

        if (firstPos.x != 7 && firstPos.y > 1
                && (theBoard[firstPos.x + 1][firstPos.y - 2] == null
                || (theBoard[firstPos.x + 1][firstPos.y - 2] != null
                && theBoard[firstPos.x + 1][firstPos.y - 2].team != thePiece.team))) {

            coordinates = new Location(firstPos.x + 1, firstPos.y - 2);
            validMoves.add(coordinates);

        }

        if (firstPos.x != 0 && firstPos.y < 6
                && (theBoard[firstPos.x - 1][firstPos.y + 2] == null
                || (theBoard[firstPos.x - 1][firstPos.y + 2] != null
                && theBoard[firstPos.x - 1][firstPos.y + 2].team != thePiece.team))) {

            coordinates = new Location(firstPos.x - 1, firstPos.y + 2);
            validMoves.add(coordinates);

        }

        if (firstPos.x != 0 && firstPos.y > 1
                && (theBoard[firstPos.x - 1][firstPos.y - 2] == null
                || (theBoard[firstPos.x - 1][firstPos.y - 2] != null
                && theBoard[firstPos.x - 1][firstPos.y - 2].team != thePiece.team))) {

            coordinates = new Location(firstPos.x - 1, firstPos.y - 2);
            validMoves.add(coordinates);

        }

        if (firstPos.x < 6 && firstPos.y != 7
                && (theBoard[firstPos.x + 2][firstPos.y + 1] == null
                || (theBoard[firstPos.x + 2][firstPos.y + 1] != null
                && theBoard[firstPos.x + 2][firstPos.y + 1].team != thePiece.team))) {

            coordinates = new Location(firstPos.x + 2, firstPos.y + 1);
            validMoves.add(coordinates);

        }

        if (firstPos.x < 6 && firstPos.y != 0
                && (theBoard[firstPos.x + 2][firstPos.y - 1] == null
                || (theBoard[firstPos.x + 2][firstPos.y - 1] != null
                && theBoard[firstPos.x + 2][firstPos.y - 1].team != thePiece.team))) {

            coordinates = new Location(firstPos.x + 2, firstPos.y - 1);
            validMoves.add(coordinates);

        }

        if (firstPos.x > 1 && firstPos.y != 7
                && (theBoard[firstPos.x - 2][firstPos.y + 1] == null
                || (theBoard[firstPos.x - 2][firstPos.y + 1] != null
                && theBoard[firstPos.x - 2][firstPos.y + 1].team != thePiece.team))) {

            coordinates = new Location(firstPos.x - 2, firstPos.y + 1);
            validMoves.add(coordinates);

        }

        if (firstPos.x > 1 && firstPos.y != 0
                && (theBoard[firstPos.x - 2][firstPos.y - 1] == null
                || (theBoard[firstPos.x - 2][firstPos.y - 1] != null
                && theBoard[firstPos.x - 2][firstPos.y - 1].team != thePiece.team))) {

            coordinates = new Location(firstPos.x - 2, firstPos.y - 1);
            validMoves.add(coordinates);

        }
        return validMoves;

    }
    
    /** Used to check the valid moves of a Bishop
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> checkMovesofBishop(Piece thePiece, Location firstPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        Location coordinates;

        int x = firstPos.x;
        int y = firstPos.y;

        while (x < 8 && y < 8) {
            x += 1;
            y += 1;
            if (x < 8 && y < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x < 8 && y > 0) {
            x += 1;
            y -= 1;
            if (x < 8 && y >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x >= 0 && y < 8) {
            x -= 1;
            y += 1;
            if (x >= 0 && y < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x >= 0 && y >= 0) {
            x -= 1;
            y -= 1;
            if (x >= 0 && y >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        return validMoves;
    }
    
    /** Used to check the valid moves of a Queen
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> checkMovesofQueen(Piece thePiece, Location firstPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        Location coordinates;

        int x = firstPos.x;
        int y = firstPos.y;

        while (x < 8 && y < 8) {
            x += 1;
            y += 1;
            if (x < 8 && y < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x < 8 && y >= 0) {
            x += 1;
            y -= 1;
            if (x < 8 && y >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x >= 0 && y < 8) {
            x -= 1;
            y += 1;
            if (x >= 0 && y < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x >= 0 && y >= 0) {
            x -= 1;
            y -= 1;
            if (x >= 0 && y >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x < 8) {
            x += 1;
            if (x < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (x >= 0) {
            x -= 1;
            if (x >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (y < 8) {
            y += 1;
            if (y < 8 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        x = firstPos.x;
        y = firstPos.y;
        while (y >= 0) {
            y -= 1;
            if (y >= 0 && (theBoard[x][y] == null || theBoard[x][y].typeofPiece.equalsIgnoreCase("king")
                    || !theBoard[x][y].team.equalsIgnoreCase(thePiece.team))) {
                coordinates = new Location(x, y);
                validMoves.add(coordinates);
            } else {
                break;
            }
        }

        return validMoves;
    }
    
    /** Used to check the valid moves of a King
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> checkMovesofKing(Piece thePiece, Location firstPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        Location coordinates;

        if (firstPos.y < 7 && (theBoard[firstPos.x][firstPos.y + 1] == null
                || !theBoard[firstPos.x][firstPos.y + 1].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x, firstPos.y + 1);
            validMoves.add(coordinates);

        }

        if ((firstPos.x < 7 && firstPos.y < 7) && (theBoard[firstPos.x + 1][firstPos.y + 1] == null
                || !theBoard[firstPos.x + 1][firstPos.y + 1].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x + 1, firstPos.y + 1);
            validMoves.add(coordinates);

        }
        if (firstPos.x < 7 && (theBoard[firstPos.x + 1][firstPos.y] == null
                || !theBoard[firstPos.x + 1][firstPos.y].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x + 1, firstPos.y);
            validMoves.add(coordinates);

        }

        if ((firstPos.x < 7 && firstPos.y > 0) && (theBoard[firstPos.x + 1][firstPos.y - 1] == null
                || !theBoard[firstPos.x + 1][firstPos.y - 1].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x + 1, firstPos.y - 1);
            validMoves.add(coordinates);

        }
        if (firstPos.y > 0 && (theBoard[firstPos.x][firstPos.y - 1] == null
                || !theBoard[firstPos.x][firstPos.y - 1].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x, firstPos.y - 1);
            validMoves.add(coordinates);

        }

        if ((firstPos.x > 0 && firstPos.y > 0) && (theBoard[firstPos.x - 1][firstPos.y - 1] == null
                || !theBoard[firstPos.x - 1][firstPos.y - 1].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x - 1, firstPos.y - 1);
            validMoves.add(coordinates);

        }
        if (firstPos.x > 0 && (theBoard[firstPos.x - 1][firstPos.y] == null
                || !theBoard[firstPos.x - 1][firstPos.y].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x - 1, firstPos.y);
            validMoves.add(coordinates);

        }

        if ((firstPos.x > 0 && firstPos.y < 7) && (theBoard[firstPos.x - 1][firstPos.y + 1] == null
                || !theBoard[firstPos.x - 1][firstPos.y + 1].team.equalsIgnoreCase(thePiece.team))) {
            coordinates = new Location(firstPos.x - 1, firstPos.y + 1);
            validMoves.add(coordinates);

        }

        if (thePiece.team.equalsIgnoreCase("white")) {
            if (whiteKingMoved == false && whiteRookMovedLeft == false) {
                if (firstPos.x == 4 && firstPos.y == 0 && theBoard[firstPos.x - 1][firstPos.y] == null
                        && theBoard[firstPos.x - 2][firstPos.y] == null
                        && theBoard[firstPos.x - 3][firstPos.y] == null) {
                    coordinates = new Location(firstPos.x - 2, firstPos.y);
                    validMoves.add(coordinates);
                }

                if (whiteKingMoved == false && whiteRookMovedRight == false) {
                    if (firstPos.x == 4 && firstPos.y == 0 && theBoard[firstPos.x + 1][firstPos.y] == null
                            && theBoard[firstPos.x + 2][firstPos.y] == null) {
                        coordinates = new Location(firstPos.x + 2, firstPos.y);
                        validMoves.add(coordinates);
                    }
                }
            }
        } else {
            if (blackKingMoved == false && blackRookMovedLeft == false) {
                if (firstPos.x == 4 && firstPos.y == 7 && theBoard[firstPos.x - 1][firstPos.y] == null
                        && theBoard[firstPos.x - 2][firstPos.y] == null
                        && theBoard[firstPos.x - 3][firstPos.y] == null) {
                    coordinates = new Location(firstPos.x - 2, firstPos.y);
                    validMoves.add(coordinates);
                }

                if (blackKingMoved == false && blackRookMovedRight == false) {
                    if (firstPos.x == 4 && firstPos.y == 7 && theBoard[firstPos.x + 1][firstPos.y] == null
                            && theBoard[firstPos.x + 2][firstPos.y] == null) {
                        coordinates = new Location(firstPos.x + 2, firstPos.y);
                        validMoves.add(coordinates);
                    }
                }
            }
        }
        return validMoves;
    }
    
    /** Checks the type of the piece to determine turn
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @param finalPos - the Position after the move
   	 * @return an int to determine where to take the state of the game
   	 */

    public static int checkTypeofPiece(Piece thePiece, Location firstPos,
            Location finalPos) {

        ArrayList<Location> validMoves = new ArrayList<Location>();
        ArrayList<Location> checkMoves = new ArrayList<Location>();
        ArrayList<Location> allAttacked = new ArrayList<Location>();
        int validMpves = 1;

        if (thePiece.typeofPiece.equalsIgnoreCase("pawn")) {

            validMoves = checkMovesofPawn(thePiece, firstPos);
            if (validMoves.contains(finalPos)) {

                movePositionofPiece(thePiece, firstPos, finalPos);
                checkMoves = checkMovesofPawn(thePiece, finalPos);
                allAttacked = beAttacked(thePiece.team);

                if (firstPos.y == 1 && finalPos.y == firstPos.y + 2) {
                    enPassantWhiteCounter = Chess.counter;
                    enPassantWhite = finalPos;
                    ePW = true;
                } else if (firstPos.y == 6 && finalPos.y == firstPos.y - 2) {
                    enPassantBlackCounter = Chess.counter;
                    enPassantBlack = finalPos;
                    ePB = true;
                } else {
                    ePB = false;
                    ePW = false;
                }

                if (firstPos.x + 1 == finalPos.x
                        && firstPos.y - 1 == finalPos.y
                        && theBoard[firstPos.x + 1][firstPos.y] != null
                        && theBoard[firstPos.x + 1][firstPos.y].typeofPiece
                        .equalsIgnoreCase("pawn")) {
                    theBoard[firstPos.x + 1][firstPos.y] = null;
                } else if (firstPos.x - 1 == finalPos.x
                        && firstPos.y - 1 == finalPos.y
                        && theBoard[firstPos.x - 1][firstPos.y] != null
                        && theBoard[firstPos.x - 1][firstPos.y].typeofPiece
                        .equalsIgnoreCase("pawn")) {
                    theBoard[firstPos.x - 1][firstPos.y] = null;
                } else if (firstPos.x - 1 == finalPos.x
                        && firstPos.y + 1 == finalPos.y
                        && theBoard[firstPos.x - 1][firstPos.y] != null
                        && theBoard[firstPos.x - 1][firstPos.y].typeofPiece
                        .equalsIgnoreCase("pawn")) {
                    theBoard[firstPos.x - 1][firstPos.y] = null;
                } else if (firstPos.x + 1 == finalPos.x
                        && firstPos.y + 1 == finalPos.y
                        && theBoard[firstPos.x + 1][firstPos.y] != null
                        && theBoard[firstPos.x + 1][firstPos.y].typeofPiece
                        .equalsIgnoreCase("pawn")) {
                    theBoard[firstPos.x + 1][firstPos.y] = null;
                }
                if (checkmateState(thePiece.team)) {
                    drawBoard();
                    System.out.println("");
                    System.out.println("Checkmate.");
                    System.out.println("");
                    if (thePiece.team.equalsIgnoreCase("white")) {
                        return 2;
                    } else if (thePiece.team.equalsIgnoreCase("black")) {
                        return 3;
                    }
                } else if (kingExists(checkMoves, thePiece.team) || kingExists(allAttacked, thePiece.team)) {
                    System.out.println("Check.");
                    System.out.println("");
                    check = true;
                }

            } else {

                badMove(validMoves);
                validMpves = 0;
            }

        } else if (thePiece.typeofPiece.equalsIgnoreCase("rook")) {

            validMoves = checkMovesofRook(thePiece, firstPos);
            if (validMoves.contains(finalPos)) {

                movePositionofPiece(thePiece, firstPos, finalPos);
                checkMoves = checkMovesofRook(thePiece, finalPos);
                allAttacked = beAttacked(thePiece.team);

                if (firstPos.equals(new Location(0, 0))) {
                    whiteRookMovedLeft = true;
                } else if (firstPos.equals(new Location(0, 7))) {
                    whiteRookMovedRight = true;
                } else if (firstPos.equals(new Location(7, 0))) {
                    blackRookMovedLeft = true;
                } else if (firstPos.equals(new Location(7, 7))) {
                    blackRookMovedRight = true;
                }

                if (checkmateState(thePiece.team)) {
                    drawBoard();
                    System.out.println("");
                    System.out.println("Checkmate.");
                    
                    System.out.println("");
                    if (thePiece.team.equalsIgnoreCase("white")) {
                        return 2;
                    } else if (thePiece.team.equalsIgnoreCase("black")) {
                        return 3;
                    }
                } else if (kingExists(checkMoves, thePiece.team) || kingExists(allAttacked, thePiece.team)) {
                    check = true;
                    System.out.println("Check.");
                    System.out.println("");
                }

            } else {

                badMove(validMoves);
                validMpves = 0;
            }

        } else if (thePiece.typeofPiece.equalsIgnoreCase("queen")) {

            validMoves = checkMovesofQueen(thePiece, firstPos);
            if (validMoves.contains(finalPos)) {

                movePositionofPiece(thePiece, firstPos, finalPos);
                checkMoves = checkMovesofQueen(thePiece, finalPos);
                allAttacked = beAttacked(thePiece.team);

                if (checkmateState(thePiece.team)) {
                    drawBoard();
                    System.out.println("");
                    System.out.println("Checkmate.");
                    System.out.println("");
                    if (thePiece.team.equalsIgnoreCase("white")) {
                        return 2;
                    } else if (thePiece.team.equalsIgnoreCase("black")) {
                        return 3;
                    }
                } else if (kingExists(checkMoves, thePiece.team) || kingExists(allAttacked, thePiece.team)) {
                    check = true;
                    System.out.println("Check.");
                    System.out.println("");
                }

            } else {

                badMove(validMoves);
                validMpves = 0;
            }

        } else if (thePiece.typeofPiece.equalsIgnoreCase("bishop")) {

            validMoves = checkMovesofBishop(thePiece, firstPos);
            if (validMoves.contains(finalPos)) {

                movePositionofPiece(thePiece, firstPos, finalPos);
                allAttacked = beAttacked(thePiece.team);
                checkMoves = checkMovesofBishop(thePiece, finalPos);
                if (checkmateState(thePiece.team)) {
                    drawBoard();
                    System.out.println("");
                    System.out.println("Checkmate.");
                    System.out.println("");
                    if (thePiece.team.equalsIgnoreCase("white")) {
                        return 2;
                    } else if (thePiece.team.equalsIgnoreCase("black")) {
                        return 3;
                    }
                } else if (kingExists(checkMoves, thePiece.team) || kingExists(allAttacked, thePiece.team)) {
                    check = true;
                    System.out.println("Check.");
                }

            } else {

                badMove(validMoves);
                validMpves = 0;
            }

        } else if (thePiece.typeofPiece.equalsIgnoreCase("knight")) {

            validMoves = checkMovesofKnight(thePiece, firstPos);
            if (validMoves.contains(finalPos)) {

                movePositionofPiece(thePiece, firstPos, finalPos);
                checkMoves = checkMovesofKnight(thePiece, finalPos);
                allAttacked = beAttacked(thePiece.team);

                if (checkmateState(thePiece.team)) {
                    drawBoard();
                    System.out.println("");
                    System.out.println("Checkmate.");
                    System.out.println("");
                    if (thePiece.team.equalsIgnoreCase("white")) {
                        return 2;
                    } else if (thePiece.team.equalsIgnoreCase("black")) {
                        return 3;
                    }
                } else if (kingExists(checkMoves, thePiece.team) || kingExists(allAttacked, thePiece.team)) {
                    check = true;
                    System.out.println("Check.");
                    System.out.println("");
                }

            } else {

                badMove(validMoves);
                validMpves = 0;
            }

        } else if (thePiece.typeofPiece.equalsIgnoreCase("king")) {

            validMoves = checkMovesofKing(thePiece, firstPos);

            checkMoves = stripKingMoves(thePiece, firstPos,
                    checkMovesofKing(thePiece, firstPos));
            allAttacked = beAttacked(thePiece.team);

            if (checkMoves.contains(finalPos)) {

                movePositionofPiece(thePiece, firstPos, finalPos);

                if (firstPos.equals(new Location(4, 0))) {
                    whiteKingMoved = true;
                } else if (firstPos.equals(new Location(4, 7))) {
                    blackKingMoved = true;
                }

                if (thePiece.team.equals("white") && firstPos.x + 2 == finalPos.x) {
                    theBoard[5][0] = theBoard[7][0];
                    theBoard[7][0] = null;
                } else if (thePiece.team.equals("white") && firstPos.x - 2 == finalPos.x) {
                    theBoard[3][0] = theBoard[0][0];
                    theBoard[0][0] = null;
                } else if (thePiece.team.equals("black") && firstPos.x + 2 == finalPos.x) {
                    theBoard[5][7] = theBoard[7][7];
                    theBoard[7][7] = null;
                } else if (thePiece.team.equals("black") && firstPos.x - 2 == finalPos.x) {
                    theBoard[3][7] = theBoard[0][7];
                    theBoard[0][7] = null;
                }

                int counter = 0;
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (theBoard[i][j] != null) {
                            counter++;

                        }
                    }
                }


                if (checkmateState(thePiece.team)) {
                    drawBoard();
                    
                    System.out.println("");
                    System.out.println("Checkmate.");
                    System.out.println("");
                    if (thePiece.team.equalsIgnoreCase("white")) {
                        return 2;
                    } else if (thePiece.team.equalsIgnoreCase("black")) {
                        return 3;
                    }
                } else if (kingExists(checkMoves, thePiece.team) || kingExists(allAttacked, thePiece.team)) {
                    check = true;
                    System.out.println("Check.");
                    System.out.println("");
                }
            } else {

                badMove(checkMoves);
                validMpves = 0;
            }
        }
        return validMpves;
    }
    
    /** Moves a piece
   	 * @param thePiece - Piece object in question
   	 * @param firstPos - the position of the piece
   	 * @param finalPos - the position you want to move the piece to
   	 */

    public static void movePositionofPiece(Piece thePiece, Location firstPos, Location finalPos) {

        if (theBoard[firstPos.x][firstPos.y] != null) {
            theBoard[finalPos.x][finalPos.y] = thePiece;
            theBoard[firstPos.x][firstPos.y] = null;
            thePiece.location = finalPos;
            Chess.counter++;
        }
    }
    
    /** Used to print a bad move in some cases
   
   	 */

    public static void badMove(ArrayList<Location> validMoves) {
    	System.out.println("");
        System.out.println("Illegal move, try again");
        System.out.println("");
    }
    
    /** Removes King moves that would put him in check/checkmate
   	 * @param thePiece - Piece object in question
   	 * @param initialPos - the position of the piece
   	 * @param possibleMoves - King's default moves
   	 * @return ArrayList of possible moves
   	 */

    public static ArrayList<Location> stripKingMoves(Piece thePiece,
            Location initialPos, ArrayList<Location> possibleMoves) {

        Location left = new Location(thePiece.location.x - 1, thePiece.location.y);
        Location topLeft = new Location(thePiece.location.x - 1,
                thePiece.location.y + 1);
        Location top = new Location(thePiece.location.x, thePiece.location.y + 1);
        Location topRight = new Location(thePiece.location.x + 1,
                thePiece.location.y + 1);
        Location right = new Location(thePiece.location.x + 1, thePiece.location.y);
        Location bottomRight = new Location(thePiece.location.x + 1,
                thePiece.location.y - 1);
        Location bottom = new Location(thePiece.location.x, thePiece.location.y - 1);
        Location bottomLeft = new Location(thePiece.location.x - 1,
                thePiece.location.y - 1);

        String color = thePiece.team;

        ArrayList<Location> allAttackedSquares;

        if (color.equalsIgnoreCase("white")) {
            allAttackedSquares = beAttacked("black");
        } else {
            allAttackedSquares = beAttacked("white");
        }

        if ((allAttackedSquares.contains(initialPos) || allAttackedSquares.contains(new Location(initialPos.x + 1, initialPos.y)) || allAttackedSquares.contains(new Location(initialPos.x + 2, initialPos.y)))) {
            possibleMoves.remove(new Location(initialPos.x + 2, initialPos.y));
        }

        if ((allAttackedSquares.contains(initialPos) || allAttackedSquares.contains(new Location(initialPos.x - 1, initialPos.y)) || allAttackedSquares.contains(new Location(initialPos.x - 2, initialPos.y)))) {
            possibleMoves.remove(new Location(initialPos.x - 2, initialPos.y));
        }

        if (!allAttackedSquares.isEmpty()) {
            if (allAttackedSquares.contains(left)) {
                possibleMoves.remove(left);
            }
            if (allAttackedSquares.contains(topLeft)) {
                possibleMoves.remove(topLeft);
            }
            if (allAttackedSquares.contains(top)) {
                possibleMoves.remove(top);
            }
            if (allAttackedSquares.contains(topRight)) {
                possibleMoves.remove(topRight);
            }
            if (allAttackedSquares.contains(right)) {
                possibleMoves.remove(right);
            }
            if (allAttackedSquares.contains(bottomRight)) {
                possibleMoves.remove(bottomRight);
            }
            if (allAttackedSquares.contains(bottom)) {
                possibleMoves.remove(bottom);
            }
            if (allAttackedSquares.contains(bottomLeft)) {
                possibleMoves.remove(bottomLeft);
            }
        }

        Piece temp = null;
        ArrayList<Location> updateAttacked;

        if (thePiece.location.x > 0
                && (theBoard[left.x][left.y] == null || !theBoard[left.x][left.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[left.x][left.y] != null) {
                temp = theBoard[left.x][left.y];
            }
            movePositionofPiece(thePiece, initialPos, left);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(left)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (left.x < 7 && left.y < 7
                    && theBoard[left.x + 1][left.y + 1] != null && (theBoard[left.x + 1][left.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (left.x > 0
                    && left.y < 7
                    && theBoard[left.x - 1][left.y + 1] != null && theBoard[left.x - 1][left.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black") && (left.x < 7
                    && left.y > 0
                    && theBoard[left.x + 1][left.y - 1] != null && theBoard[left.x + 1][left.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (left.x > 0
                    && left.y > 0
                    && theBoard[left.x - 1][left.y - 1] != null && theBoard[left.x - 1][left.y - 1].nameofPiece
                    .equalsIgnoreCase("wp")))) {

                possibleMoves.remove(left);
                movePositionofPiece(thePiece, left, initialPos);
                theBoard[left.x][left.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, left, initialPos);
                theBoard[left.x][left.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.x > 0
                && thePiece.location.y < 7
                && (theBoard[topLeft.x][topLeft.y] == null || !theBoard[topLeft.x][topLeft.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[topLeft.x][topLeft.y] != null) {
                temp = theBoard[topLeft.x][topLeft.y];
            }
            movePositionofPiece(thePiece, initialPos, topLeft);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(topLeft)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (topLeft.x < 7
                    && topLeft.y < 7
                    && theBoard[topLeft.x + 1][topLeft.y + 1] != null && (theBoard[topLeft.x + 1][topLeft.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (topLeft.x > 0
                    && topLeft.y < 7
                    && theBoard[topLeft.x - 1][topLeft.y + 1] != null && theBoard[topLeft.x - 1][topLeft.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (topLeft.x < 7
                    && topLeft.y > 0
                    && theBoard[topLeft.x + 1][topLeft.y - 1] != null && (theBoard[topLeft.x + 1][topLeft.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (topLeft.x > 0
                    && topLeft.y > 0
                    && theBoard[topLeft.x - 1][topLeft.y - 1] != null && theBoard[topLeft.x - 1][topLeft.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(topLeft);
                movePositionofPiece(thePiece, topLeft, initialPos);
                theBoard[topLeft.x][topLeft.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, topLeft, initialPos);
                theBoard[topLeft.x][topLeft.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.y < 7
                && (theBoard[top.x][top.y] == null || !theBoard[top.x][top.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[top.x][top.y] != null) {
                temp = theBoard[top.x][top.y];
            }
            movePositionofPiece(thePiece, initialPos, top);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(top)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (top.x < 7 && top.y < 7
                    && theBoard[top.x + 1][top.y + 1] != null && (theBoard[top.x + 1][top.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (top.x > 0
                    && top.y < 7 && theBoard[top.x - 1][top.y + 1] != null && theBoard[top.x - 1][top.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (top.x < 7 && top.y > 0
                    && theBoard[top.x + 1][top.y - 1] != null && (theBoard[top.x + 1][top.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (top.x > 0
                    && top.y > 0 && theBoard[top.x - 1][top.y - 1] != null && theBoard[top.x - 1][top.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(top);
                movePositionofPiece(thePiece, top, initialPos);
                theBoard[top.x][top.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, top, initialPos);
                theBoard[top.x][top.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.y < 7
                && thePiece.location.x < 7
                && (theBoard[topRight.x][topRight.y] == null || !theBoard[topRight.x][topRight.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[topRight.x][topRight.y] != null) {
                temp = theBoard[topRight.x][topRight.y];
            }
            movePositionofPiece(thePiece, initialPos, topRight);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(topRight)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (topRight.x < 7
                    && topRight.y < 7
                    && theBoard[topRight.x + 1][topRight.y + 1] != null && (theBoard[topRight.x + 1][topRight.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (topRight.x > 0
                    && topRight.y < 7
                    && theBoard[topRight.x - 1][topRight.y + 1] != null && theBoard[topRight.x - 1][topRight.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (topRight.x < 7
                    && topRight.y > 0
                    && theBoard[topRight.x + 1][topRight.y - 1] != null && (theBoard[topRight.x + 1][topRight.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (topRight.x > 0
                    && topRight.y > 0
                    && theBoard[topRight.x - 1][topRight.y - 1] != null && theBoard[topRight.x - 1][topRight.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(topRight);
                movePositionofPiece(thePiece, topRight, initialPos);
                theBoard[topRight.x][topRight.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, topRight, initialPos);
                theBoard[topRight.x][topRight.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.x < 7
                && (theBoard[right.x][right.y] == null || !theBoard[right.x][right.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[right.x][right.y] != null) {
                temp = theBoard[right.x][right.y];
            }
            movePositionofPiece(thePiece, initialPos, right);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(right)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (right.x < 7 && right.y < 7
                    && theBoard[right.x + 1][right.y + 1] != null && (theBoard[right.x + 1][right.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (right.x > 0
                    && right.y < 7
                    && theBoard[right.x - 1][right.y + 1] != null && theBoard[right.x - 1][right.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (right.x < 7 && right.y > 0
                    && theBoard[right.x + 1][right.y - 1] != null && (theBoard[right.x + 1][right.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (right.x > 0
                    && right.y > 0
                    && theBoard[right.x - 1][right.y - 1] != null && theBoard[right.x - 1][right.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(right);
                movePositionofPiece(thePiece, right, initialPos);
                theBoard[right.x][right.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, right, initialPos);
                theBoard[right.x][right.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.x < 7
                && thePiece.location.y > 0
                && (theBoard[bottomRight.x][bottomRight.y] == null || !theBoard[bottomRight.x][bottomRight.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[bottomRight.x][bottomRight.y] != null) {
                temp = theBoard[bottomRight.x][bottomRight.y];
            }
            movePositionofPiece(thePiece, initialPos, bottomRight);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(bottomRight)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (bottomRight.x < 7
                    && bottomRight.y < 7
                    && theBoard[bottomRight.x + 1][bottomRight.y + 1] != null && (theBoard[bottomRight.x + 1][bottomRight.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (bottomRight.x > 0
                    && bottomRight.y < 7
                    && theBoard[bottomRight.x - 1][bottomRight.y + 1] != null && theBoard[bottomRight.x - 1][bottomRight.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (bottomRight.x < 7
                    && bottomRight.y > 0
                    && theBoard[bottomRight.x + 1][bottomRight.y - 1] != null && (theBoard[bottomRight.x + 1][bottomRight.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (bottomRight.x > 0
                    && bottomRight.y > 0
                    && theBoard[bottomRight.x - 1][bottomRight.y - 1] != null && theBoard[bottomRight.x - 1][bottomRight.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(bottomRight);
                movePositionofPiece(thePiece, bottomRight, initialPos);
                theBoard[bottomRight.x][bottomRight.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, bottomRight, initialPos);
                theBoard[bottomRight.x][bottomRight.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.y > 0
                && (theBoard[bottom.x][bottom.y] == null || !theBoard[bottom.x][bottom.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[bottom.x][bottom.y] != null) {
                temp = theBoard[bottom.x][bottom.y];
            }
            movePositionofPiece(thePiece, initialPos, bottom);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(bottom)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (bottom.x < 7
                    && bottom.y < 7
                    && theBoard[bottom.x + 1][bottom.y + 1] != null && (theBoard[bottom.x + 1][bottom.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (bottom.x > 0
                    && bottom.y < 7
                    && theBoard[bottom.x - 1][bottom.y + 1] != null && theBoard[bottom.x - 1][bottom.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (bottom.x < 7
                    && bottom.y > 0
                    && theBoard[bottom.x + 1][bottom.y - 1] != null && (theBoard[bottom.x + 1][bottom.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (bottom.x > 0
                    && bottom.y > 0
                    && theBoard[bottom.x - 1][bottom.y - 1] != null && theBoard[bottom.x - 1][bottom.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(bottom);
                movePositionofPiece(thePiece, bottom, initialPos);
                theBoard[bottom.x][bottom.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, bottom, initialPos);
                theBoard[bottom.x][bottom.y] = temp;
                temp = null;
            }
        }

        if (thePiece.location.y > 0
                && thePiece.location.x > 0
                && (theBoard[bottomLeft.x][bottomLeft.y] == null || !theBoard[bottomLeft.x][bottomLeft.y].team
                .equalsIgnoreCase(thePiece.team))) {
            if (theBoard[bottomLeft.x][bottomLeft.y] != null) {
                temp = theBoard[bottomLeft.x][bottomLeft.y];
            }
            movePositionofPiece(thePiece, initialPos, bottomLeft);

            if (thePiece.team.equalsIgnoreCase("white")) {
                updateAttacked = beAttacked("black");
            } else {
                updateAttacked = beAttacked("white");
            }

            if (updateAttacked.contains(bottomLeft)
                    || ((thePiece.team.equalsIgnoreCase("white")
                    && (bottomLeft.x < 7
                    && bottomLeft.y < 7
                    && theBoard[bottomLeft.x + 1][bottomLeft.y + 1] != null && (theBoard[bottomLeft.x + 1][bottomLeft.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))) || (bottomLeft.x > 0
                    && bottomLeft.y < 7
                    && theBoard[bottomLeft.x - 1][bottomLeft.y + 1] != null && theBoard[bottomLeft.x - 1][bottomLeft.y + 1].nameofPiece
                    .equalsIgnoreCase("bp"))))
                    || ((thePiece.team.equalsIgnoreCase("black")
                    && (bottomLeft.x < 7
                    && bottomLeft.y > 0
                    && theBoard[bottomLeft.x + 1][bottomLeft.y - 1] != null && (theBoard[bottomLeft.x + 1][bottomLeft.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))) || (bottomLeft.x > 0
                    && bottomLeft.y > 0
                    && theBoard[bottomLeft.x - 1][bottomLeft.y - 1] != null && theBoard[bottomLeft.x - 1][bottomLeft.y - 1].nameofPiece
                    .equalsIgnoreCase("wp"))))) {
                possibleMoves.remove(bottomLeft);
                movePositionofPiece(thePiece, bottomLeft, initialPos);
                theBoard[bottomLeft.x][bottomLeft.y] = temp;
                temp = null;
            } else {
                movePositionofPiece(thePiece, bottomLeft, initialPos);
                theBoard[bottomLeft.x][bottomLeft.y] = temp;
                temp = null;
            }
        }

        return possibleMoves;
    }

}
