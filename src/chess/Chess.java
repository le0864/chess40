/** Executes the Chess Program and works through the logic of commands.
 *
 * @author Peter Lambe
 * @author Le Liu
 * 
 */

package chess;

import data.Board;
import data.*;
import java.util.*;

public class Chess {

	public static int counter = 0;

	
	/** Converts a given String to an int for move parsing
   	 * @param letter - String to be converted
   	 * @return converted int
   	 */
	public static int customStringtoIntConverter(String letter) {

		int coordinates;

		if (letter.equalsIgnoreCase("a") || letter.equalsIgnoreCase("1"))
			coordinates = 0;
		else if (letter.equalsIgnoreCase("b") || letter.equalsIgnoreCase("2"))
			coordinates = 1;
		else if (letter.equalsIgnoreCase("c") || letter.equalsIgnoreCase("3"))
			coordinates = 2;
		else if (letter.equalsIgnoreCase("d") || letter.equalsIgnoreCase("4"))
			coordinates = 3;
		else if (letter.equalsIgnoreCase("e") || letter.equalsIgnoreCase("5"))
			coordinates = 4;
		else if (letter.equalsIgnoreCase("f") || letter.equalsIgnoreCase("6"))
			coordinates = 5;
		else if (letter.equalsIgnoreCase("g") || letter.equalsIgnoreCase("7"))
			coordinates = 6;
		else if (letter.equalsIgnoreCase("h") || letter.equalsIgnoreCase("8"))
			coordinates = 7;
		else
			coordinates = -1;

		return coordinates;
	}
	
	/** Initializes all of the Chess Pieces upon start up
   	 */

	public static void createChessPieces() {

		Board.theBoard[0][0] = new Piece("rook", "white", "wR", new Location(0, 0));
		Board.theBoard[1][0] = new Piece("knight", "white", "wN", new Location(1, 0));
		Board.theBoard[2][0] = new Piece("bishop", "white", "wB", new Location(2, 0));
		Board.theBoard[3][0] = new Piece("queen", "white", "wQ", new Location(3, 0));
		Board.theBoard[4][0] = new Piece("king", "white", "wK", new Location(4, 0));
		Board.theBoard[5][0] = new Piece("bishop", "white", "wB", new Location(5, 0));
		Board.theBoard[6][0] = new Piece("knight", "white", "wN", new Location(6, 0));
		Board.theBoard[7][0] = new Piece("rook", "white", "wR", new Location(7, 0));
		Board.theBoard[0][1] = new Piece("pawn", "white", "wp", new Location(0, 1));
		Board.theBoard[1][1] = new Piece("pawn", "white", "wp", new Location(1, 1));
		Board.theBoard[2][1] = new Piece("pawn", "white", "wp", new Location(2, 1));
		Board.theBoard[3][1] = new Piece("pawn", "white", "wp", new Location(3, 1));
		Board.theBoard[4][1] = new Piece("pawn", "white", "wp", new Location(4, 1));
		Board.theBoard[5][1] = new Piece("pawn", "white", "wp", new Location(5, 1));
		Board.theBoard[6][1] = new Piece("pawn", "white", "wp", new Location(6, 1));
		Board.theBoard[7][1] = new Piece("pawn", "white", "wp", new Location(7, 1));

		Board.theBoard[0][7] = new Piece("rook", "black", "bR", new Location(0, 7));
		Board.theBoard[1][7] = new Piece("knight", "black", "bN", new Location(1, 7));
		Board.theBoard[2][7] = new Piece("bishop", "black", "bB", new Location(2, 7));
		Board.theBoard[3][7] = new Piece("queen", "black", "bQ", new Location(3, 7));
		Board.theBoard[4][7] = new Piece("king", "black", "bK", new Location(4, 7));
		Board.theBoard[5][7] = new Piece("bishop", "black", "bB", new Location(5, 7));
		Board.theBoard[6][7] = new Piece("knight", "black", "bN", new Location(6, 7));
		Board.theBoard[7][7] = new Piece("rook", "black", "bR", new Location(7, 7));
		Board.theBoard[0][6] = new Piece("pawn", "black", "bp", new Location(0, 6));
		Board.theBoard[1][6] = new Piece("pawn", "black", "bp", new Location(1, 6));
		Board.theBoard[2][6] = new Piece("pawn", "black", "bp", new Location(2, 6));
		Board.theBoard[3][6] = new Piece("pawn", "black", "bp", new Location(3, 6));
		Board.theBoard[4][6] = new Piece("pawn", "black", "bp", new Location(4, 6));
		Board.theBoard[5][6] = new Piece("pawn", "black", "bp", new Location(5, 6));
		Board.theBoard[6][6] = new Piece("pawn", "black", "bp", new Location(6, 6));
		Board.theBoard[7][6] = new Piece("pawn", "black", "bp", new Location(7, 6));
	}

	/** Main method to start the game and handle logic
	 * @throws basic exceptions for error handling
   	 */
	public static void main(String[] args) throws Exception {
		createChessPieces();
		Board chessBoard = new Board(new Piece[8][8]);

		String line = null;
		int firstX = -1;
		int firstY = -1;
		int endX = -1;
		int endY = -1;
		String whoseMove = "white";
		int possible = 0;
		boolean tie = false;
		
		while (chessBoard.kingStillAlive().equalsIgnoreCase("both")) {
			
			Board.drawBoard();

			if (whoseMove.equalsIgnoreCase("white")){
				System.out.println("");
				System.out.print("White's Move: ");
				System.out.println("");
			}
			else {
				System.out.println("");
				System.out.print("Black's Move: ");
				System.out.println("");
			}
			
			Scanner sc = new Scanner(System.in);

			try{
				line = sc.nextLine();
			} catch(NoSuchElementException e){
				System.exit(1);
			}
			
			String arr[] = line.split(" ");

			if (arr.length >= 2) {
				firstX = customStringtoIntConverter(arr[0].substring(0, 1));
				firstY = customStringtoIntConverter(arr[0].substring(1));
				endX = customStringtoIntConverter(arr[1].substring(0, 1));
				endY = customStringtoIntConverter(arr[1].substring(1));
			}

			if (arr.length >= 2
					&& ((firstX < 0 || firstX > 7)
							|| (firstY < 0 || firstY > 7)
							|| (endX < 0 || endX > 7) || (endX < 0 || endX > 7))) {
				System.out.println("");
				System.out.println("Illegal move, try again");
				System.out.println("");
			} else if(arr.length == 2 && firstX == endX && firstY == endY){
				System.out.println("");
				System.out.println("Illegal move, try again");
				System.out.println("");
			}else {

				if (tie) {
					if (arr.length == 1) {
						if (arr[0].equalsIgnoreCase("draw")) {
							System.out.println("");
							System.out.println("Draw");
							System.out.println("");
							break;
						}
					}
				}

				if (arr.length == 1 && arr[0].equalsIgnoreCase("resign")) {
					if (whoseMove.equalsIgnoreCase("white")) {
						System.out.println("");
						System.out.println("Black wins");
						System.out.println("");
						break;
					} else {
						System.out.println("");
						System.out.println("White wins");
						System.out.println("");
						break;
					}
				} else if (arr.length >= 2 && Board.theBoard[firstX][firstY] != null
						&& Board.theBoard[firstX][firstY].team
								.equalsIgnoreCase("white")
						&& whoseMove.equalsIgnoreCase("white")) {
					tie = false;
					possible = Board.checkTypeofPiece(
							Board.theBoard[firstX][firstY], new Location(
									firstX, firstY), new Location(endX,
									endY));
					
					if (possible==1)
						whoseMove = "black";
					else if(possible==0)
						whoseMove = "white";
					else if(possible==2){
						System.out.println("");
						System.out.println("White wins");
						System.out.println("");
						break;
					} else if(possible==3){
						System.out.println("");
						System.out.println("Black wins");
						System.out.println("");
						break;
					} else if(possible==4){
						break;
					}
					
					if (arr.length == 3 && arr[2].equalsIgnoreCase("draw?")) {
						System.out.println("");
						System.out.println("Type draw to accept the opponent's draw offer.");
						System.out.println("");
						tie = true;
						
					} else if (arr.length == 2 && firstY == 6 && Board.theBoard[endX][endY].typeofPiece.equalsIgnoreCase("pawn")) {
						Board.theBoard[endX][endY].nameofPiece = Board.theBoard[endX][endY].team
								.substring(0, 1) + "Q";
						
						ArrayList<Location> checkMoves = new ArrayList<Location>();
						Board.theBoard[endX][endY].typeofPiece = "queen";
						checkMoves = Board.checkMovesofQueen(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
						if (Board.checkmateState("white")) {
							Board.drawBoard();
							System.out.println("");
							System.out.println("Checkmate.");
							System.out.println("White wins!");
							System.out.println("");
							break;
						} else if (Board.kingExists(checkMoves,Board.theBoard[endX][endY].team)) {
							System.out.println("");
							System.out.println("Check.");
							System.out.println("");
						}
						
					} else if (arr.length == 3
							&& firstY == 6
							&& (arr[2].equalsIgnoreCase("b")
									|| arr[2].equalsIgnoreCase("n")
									|| arr[2].equalsIgnoreCase("r") || arr[2]
										.equalsIgnoreCase("q"))) {
						Board.theBoard[endX][endY].nameofPiece = Board.theBoard[endX][endY].team
								.substring(0, 1) + arr[2].toUpperCase();
						ArrayList<Location> checkMoves = new ArrayList<Location>();
						
						if (arr[2].equalsIgnoreCase("b")) {
							Board.theBoard[endX][endY].typeofPiece = "bishop";
							checkMoves = Board.checkMovesofBishop(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("white")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("White wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checkMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("");
								System.out.println("Check");
								System.out.println("");
							} 
						} else if (arr[2].equalsIgnoreCase("n")) {
							Board.theBoard[endX][endY].typeofPiece = "knight";
							checkMoves = Board.checkMovesofKnight(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("white")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("White wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checkMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("");
								System.out.println("Check");
								System.out.println("");
							} 
						} else if (arr[2].equalsIgnoreCase("r")) {
							Board.theBoard[endX][endY].typeofPiece = "rook";
							checkMoves = Board.checkMovesofRook(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("white")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("White wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checkMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check");
							} 
						} else if (arr[2].equalsIgnoreCase("q")) {
							Board.theBoard[endX][endY].typeofPiece = "queen";
							checkMoves = Board.checkMovesofQueen(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("white")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("White wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checkMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check");
								System.out.println("");
							} 
						}
						
					} else if (arr.length == 3) {
						System.out.println("");
						System.out.println("Illegal move, try again");
						System.out.println("");
						whoseMove = "white";
					}
					
				} else if (arr.length >= 2 && Board.theBoard[firstX][firstY] != null
						&& Board.theBoard[firstX][firstY].team
								.equalsIgnoreCase("black")
						&& whoseMove.equalsIgnoreCase("black")) {
					tie = false;
					possible = Board.checkTypeofPiece(
							Board.theBoard[firstX][firstY], new Location(
									firstX, firstY), new Location(endX,
									endY));
					
					if (possible==1)
						whoseMove = "white";
					else if(possible==0)
						whoseMove = "black";
					else if(possible==2){
						System.out.println("");
						System.out.println("White wins");
						System.out.println("");
						break;
					} else if(possible==3){
						System.out.println("");
						System.out.println("Black wins");
						System.out.println("");
						break;
					} else if(possible==4){
						break;
					}

					if (arr.length == 3 && arr[2].equalsIgnoreCase("draw?")) {
						System.out.println("");
						System.out.println("Type draw to accept the opponent's draw offer.");
						System.out.println("");
						tie = true;
						
					} else if (arr.length == 2 && firstY == 1 && Board.theBoard[endX][endY].typeofPiece.equalsIgnoreCase("pawn")) {
							Board.theBoard[endX][endY].nameofPiece = Board.theBoard[endX][endY].team
									.substring(0, 1) + "Q";
							
							ArrayList<Location> checkMoves = new ArrayList<Location>();
							Board.theBoard[endX][endY].typeofPiece = "queen";
							checkMoves = Board.checkMovesofQueen(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("black")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("Black wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checkMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check");
								System.out.println("");
							}
							
					} else if (arr.length == 3
							&& firstY == 1
							&& (arr[2].equalsIgnoreCase("b")
									|| arr[2].equalsIgnoreCase("n")
									|| arr[2].equalsIgnoreCase("r") || arr[2]
										.equalsIgnoreCase("q"))) {
						Board.theBoard[endX][endY].nameofPiece = Board.theBoard[endX][endY].team.substring(0, 1) + arr[2].toUpperCase();

						ArrayList<Location> checktheMoves = new ArrayList<Location>();
								
						if (arr[2].equalsIgnoreCase("b")) {
							Board.theBoard[endX][endY].typeofPiece = "bishop";
							checktheMoves = Board.checkMovesofBishop(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("black")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("Black wins");
								
								System.out.println("");
								break;
							} else if (Board.kingExists(checktheMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check");
								System.out.println("");
							}
						} else if (arr[2].equalsIgnoreCase("n")) {
							Board.theBoard[endX][endY].typeofPiece = "knight";
							checktheMoves = Board.checkMovesofKnight(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("black")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("Black wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checktheMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check");
								System.out.println("");
							} 
						} else if (arr[2].equalsIgnoreCase("r")) {
							Board.theBoard[endX][endY].typeofPiece = "rook";
							checktheMoves = Board.checkMovesofRook(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							if (Board.checkmateState("black")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("Black wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checktheMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check.");
							} 
						} else if (arr[2].equalsIgnoreCase("q")) {
							Board.theBoard[endX][endY].typeofPiece = "queen";

							checktheMoves = Board.checkMovesofQueen(Board.theBoard[endX][endY], Board.theBoard[endX][endY].location);
							
							if (Board.checkmateState("black")) {
								Board.drawBoard();
								System.out.println("");
								System.out.println("Checkmate");
								System.out.println("Black wins");
								System.out.println("");
								break;
							} else if (Board.kingExists(checktheMoves,Board.theBoard[endX][endY].team)) {
								System.out.println("Check");
								System.out.println("");
							} 
						}

					} else if (arr.length == 3) {
						System.out.println("");
						System.out.println("Illegal move, try again");
						System.out.println("");
						whoseMove = "black";
					}

				} else if (arr.length == 2 && Board.theBoard[firstX][firstY] != null
						&& Board.theBoard[firstX][firstY].team
								.equalsIgnoreCase("white")
						&& whoseMove.equalsIgnoreCase("black")) {
					System.out.println("");
					System.out.println("Illegal move, try again");
					System.out.println("");
					whoseMove = "black";
				} else if (arr.length == 2 && Board.theBoard[firstX][firstY] != null
						&& Board.theBoard[firstX][firstY].team
								.equalsIgnoreCase("black")
						&& whoseMove.equalsIgnoreCase("white")) {
					System.out.println("");
					System.out.println("Illegal move, try again");
					System.out.println("");
					whoseMove = "white";
				} else {
					System.out.println("");
					System.out.println("Illegal move, try again");
					System.out.println("");
				}
			}
		}
		if(chessBoard.kingStillAlive().equalsIgnoreCase("white")) System.out.println("White wins");
		else if(chessBoard.kingStillAlive().equalsIgnoreCase("black")) System.out.println("Black wins");
	}

		
		

}
